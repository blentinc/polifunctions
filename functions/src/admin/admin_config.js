//holi
const admin = require("firebase-admin")

const urlProd = "https://poli-1307-atw.firebaseio.com"
const urlDev = "https://polidev.firebaseio.com"

const servAccountProd = "../certificates/seraccprod.json"
const servAccountDev = "../certificates/seracc.json"


// CONFIGURAR SEGUN EL AMBIENTE
const serviceAccount = require(servAccountProd);
const url = urlProd

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: url
});

exports.globalAdmin = admin

exports.db = admin.database();