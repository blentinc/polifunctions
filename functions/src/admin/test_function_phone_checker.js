exports.__esModule = true
const admin = require("./admin_config");
var db = admin.db;

check_phones()

async function check_phones() {

    try {
        //1. traiga los telefonos que el cliente tiene
        //2. compare cada telefono con lo que hay en db
        //3. si el telefono esta en db agregelo a my_contacts 
        //4. borre la rama de check_phones

        const uid = 'FdLAy43n2LVqDvb4ATMHLLbSrnF3'
        // ESTE QUERY NO VA A IR EN LA FUNCION
        const query1 = db.ref().child('check_phones')       
                               .child('FdLAy43n2LVqDvb4ATMHLLbSrnF3')
                               .child('-M-XjTMmz6mL3_UnIxe8')

        const result1 = await query1.once('value')
        //const myPhones = Object.values(result1.val())
        const myPhones = ['+573114821372']

        const multiPathObj = {}

        console.time('F1')
        for (i = 0; i < myPhones.length; i++) {

            const phone = myPhones[i]
            const check_status_query = db.ref().child('phones')
                                               .orderByValue()
                                               .equalTo(phone)
                                               
            const result2 = await check_status_query.once('value')

            
            if  (result2.val()) {
                // esta ruta debe ser my_contacts/${uid}/${uid_contact} = "uid" 
                //aca hay que hacer otro await 
                console.log('El telefono esta en poli => ',phone)
                multiPathObj[`my_contacts/${uid}/${phone}`] = phone
            }
        }
        console.timeEnd('F1')

        process.exit()

    } catch (error) {
        console.log('Error in function phone_checker => ',error)
    }

}