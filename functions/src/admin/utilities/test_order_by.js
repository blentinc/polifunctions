exports.__esModule = true
const admin = require("../admin_config");

var db = admin.db;

getOnceValueFromPathLimited('posts_group/-M2eqGsNOLimzmwH2dsZ',2)

async function getOnceValueFromPathLimited(path,limit) {
    const tag = "getOnceValueFromPath"
    //tools.logFunctionCalled(tag)

    try {
        const query = db.ref()
                        .child(path)
                        .orderByKey()
                        .limitToLast(limit)                      
        const result =  await query.once('value')
        console.log('Result => ',result.val())
        return result.val()
       
    } catch (error) {
        //tools.logErrorInFunction(tag,error)
        return error
    }   
}