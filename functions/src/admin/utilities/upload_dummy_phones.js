exports.__esModule = true
const admin = require("../admin_config");
const data = require('./data.json');

var db = admin.db;

uploadDummyPhones()

async function uploadDummyPhones() {

    try {
        const dummyUID = (await db.ref().push()).key
        const dataArray = data

        const multiPathObj = {}

        console.log("tienes ",data.length,"numeros en tu phone book")

        for (i = 0; i < dataArray.length; i++) {
            const phone = dataArray[i]

            multiPathObj[`phone_book/${dummyUID}/${phone}`] = phone
        }

        await db.ref().update(multiPathObj);
        process.exit()
        
    } catch (error) {
        
    }

}