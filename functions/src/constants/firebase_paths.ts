export const KEY_MEMBER_COUNT = "count_members"
export const KEY_MY_GROUPS_COUNT = "count_my_groups"

export const PATH_PHONES = "phone_active_users"
export const PATH_PHONE_BOOK = "phone_book"
export const PATH_PHONE_BOOK_INDEX = "phone_book_index"
export const PATH_PEOPLE = "people"
export const PATH_MY_CONTACTS = "my_contacts"
export const PATH_MY_CONTACTS_INDEX = "my_contacts_index"
export const PATH_GROUPS = "groups";
export const PATH_GROUP_MEMBERS = "group_members"
export const PATH_MY_GROUPS = "my_groups"
export const PATH_GROUP_ADMINS = "group_admins"
export const PATH_TOKENS = "tokens"
export const PATH_POSTS = "posts";
// rutas para escritura en feed del grupo
export const PATH_POSTS_GROUP = "posts_group";//tiene todos los posts del grupo => control
export const PATH_POSTS_GROUP_EV = "posts_group_ev";//posts que tienen eventos => control
export const PATH_POSTS_GROUP_EV_PRIV = "posts_group_ev_priv";//eventos privados => control
export const PATH_POSTS_GROUP_PRIV = "posts_group_priv";//tiene las publicaciones privadas => control
// estas las podria ver un usuairo nuevo
export const PATH_POSTS_GROUP_EV_PUB = "posts_group_ev_pub";//eventos publicos del grupo => lo veria un usuario nuevo
export const PATH_POSTS_GROUP_PUB = "posts_group_pub";//tiene los posts que son publicos => lo veria un usuario nuevo
// rutas para escritura en feed de usuario
export const PATH_FEED = "feed";
export const PATH_FEED_EVENTS = "feed_events";
export const PATH_FEED_BY_GROUP = "feed_by_group";
export const PATH_REPLIES = "replies";

