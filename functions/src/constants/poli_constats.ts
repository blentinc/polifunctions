export const ROL_FOUNDER = "founder"
export const ROL_ADMIN = "admin"
export const KEY_GROUP = "group"
export const KEY_ALL = "all"

export const KEY_MSG = "msg"
export const KEY_TITLE = "title"

export enum notificationType {
    post,
    reply,
    added,
    admin,
    chat
}

export enum postType {
    public,
    private,
    general
}