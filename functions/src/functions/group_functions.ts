import * as functions from 'firebase-functions'
import * as paths from '../constants/firebase_paths'
import * as groupTools from '../tools/group_tools'
import * as fcmTools from '../tools/fcm_tools'

export const handleNewMemberInGroup =
    functions
        .database
        .ref(`${paths.PATH_GROUP_MEMBERS}/{groupId}/{uid}`)
        .onCreate(async (change, context) => {

            const value = change.val()
            const uid = context.params.uid
            const groupId = context.params.groupId

            const tasks = []

            tasks.push(groupTools.writeGroupToMyGroups(uid, groupId, value))
            tasks.push(groupTools.addMemberCount(groupId))
            tasks.push(fcmTools.subscribeToTopic(groupId, uid))
            tasks.push(groupTools.addMyGroupsCount(uid))
            tasks.push(groupTools.writeGroupPostsToNewMember(uid, groupId))
            tasks.push(groupTools.sendWelcomeNotification(groupId,uid))

            // traer contenido del grupo al miembro
            // pendiente esta

            try {
                await Promise.all(tasks)
            } catch (error) {
                console.log(error)
            }
        })

export const handleRemoveMemberFromGroup =
    functions
        .database
        .ref(`${paths.PATH_GROUP_MEMBERS}/{groupId}/{uid}`)
        .onDelete(async (change, context) => {

            const uid = context.params.uid
            const groupId = context.params.groupId

            const tasks = []

            tasks.push(groupTools.removePersonFromGroup(uid, groupId))
            tasks.push(fcmTools.unsubscribeFromTopic(groupId, uid))
            tasks.push(groupTools.removeMemberCount(groupId))
            tasks.push(groupTools.removeMyGroupsCount(uid))

            try {
                await Promise.all(tasks)
            } catch (error) {
                console.log(error)
            }
        })

export const handleDeleteGroup =
    functions
        .database
        .ref(`${paths.PATH_GROUPS}/{groupId}`)
        .onDelete(async (change, context) => {

            const groupId = context.params.groupId
            const group = change.val()

            try {
                await groupTools.deleteGroup(groupId, group)
            } catch (error) {
                console.log(error)
            }
        })

export const handleAdminPromotion =
        functions
            .database
            .ref(`${paths.PATH_GROUP_MEMBERS}/{groupId}/{uid}`)
            .onUpdate(async (change, context) => {
                
                const uid = context.params.uid
                const groupId = context.params.groupId
                const value = change.after.val()
    
                try {
                    await groupTools.sendAdminNotification(value,groupId,uid)
                } catch (error) {
                    console.log(error)
                }
            })



