import * as functions from 'firebase-functions'
import * as paths from '../constants/firebase_paths'
import * as tools from '../tools/tools'
import * as dbTools from '../tools/database_tools'
import * as peopleTools from '../tools/people_tools'
import * as groupTools from '../tools/group_tools'

export const handleUsernameUpdated =
    functions
        .database
        .ref(`${paths.PATH_PEOPLE}/{uid}/name_lowercased`)
        .onUpdate(async (change, context) => {

            const name = change.after.val()
            const nameJoined = tools.removeSpacesFromWord(name)
            const uid = context.params.uid
            const multiPathObj = {} as any
            const querys = []
        
            try {

                querys.push(peopleTools.getMyContactsIndex(uid))
                querys.push(groupTools.getMyGroups(uid))

                const promises = await Promise.all(querys)
                
                const uids = promises[0]
                if (uids) {
                    peopleTools.updateNameInMyContacts(uid,uids,nameJoined,multiPathObj)
                }

                const groupIds = promises[1]
                if (groupIds) {
                    peopleTools.updateNameInMyGroups(uid,groupIds,nameJoined,multiPathObj)
                }

                if (!tools.isObjectEmpty(multiPathObj)) {
                    await dbTools.runMultiPathUpdate(multiPathObj)
                }
                
            } catch (error) {
                console.log(error)
            }
})


