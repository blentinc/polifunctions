import * as functions from 'firebase-functions'
import * as pahts from '../constants/firebase_paths'
import * as tools from '../tools/tools'
import * as phoneTools from '../tools/phone_tools'
import * as peopleTools from '../tools/people_tools'


export const handleCreatedPhoneInPhoneBook =
    functions
        .database
        .ref(`${pahts.PATH_PHONE_BOOK}/{uid}/{phone}`)
        .onCreate(async (change, context) => {

            const phone = change.val()
            const uid = context.params.uid

            try {

                await phoneTools.writePhoneToPhoneBookIndex(phone, uid)

                const phoneUid = await phoneTools.checkPhoneStatus(phone)

                if (phoneUid) {
                    tools.logMsgValue(`The phone is in Poli => ${phone}`, phoneUid)
                    await peopleTools.writePersonToMyContacts(uid, phoneUid)
                }

            } catch (error) {
                console.log(error)
            }


        })

export const handleDeletePhoneInPhoneBook =
    functions
        .database
        .ref(`${pahts.PATH_PHONE_BOOK}/{uid}/{phone}`)
        .onDelete(async (change, context) => {

            const phone = change.val()
            const uid = context.params.uid

            try {
                await phoneTools.deletePhoneOfPhoneBookIndex(phone, uid)

                const phoneUid = await phoneTools.checkPhoneStatus(phone)

                if (phoneUid) {
                    tools.logMsgValue(`The phone is in Poli => ${phone}`, phoneUid)
                    await peopleTools.deletePersonOfMyContacts(uid, phoneUid)
                }

            } catch (error) {
                console.log(error)
            }


        })