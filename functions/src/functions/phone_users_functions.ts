import * as functions from 'firebase-functions'
import * as pahts from '../constants/firebase_paths'
import * as phoneTools from '../tools/phone_tools'
import * as peopleTools from '../tools/people_tools'

export const handleNewPhoneInPhoneActiveUsers =
    functions
        .database
        .ref(`${pahts.PATH_PHONES}/{uid}`)
        .onCreate(async (change, context) => {

            const phone = change.val()
            const uidContact = context.params.uid
            const uids = await phoneTools.getPhoneIndexdedUids(phone)
            const promises = []

            for (const uid of uids) {
                promises.push(peopleTools.writePersonToMyContacts(uid, uidContact))
            }

            try {
                await Promise.all(promises)
            } catch (error) {
                console.log(error)
            }

        })