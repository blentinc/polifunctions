import * as functions from 'firebase-functions'
import * as paths from '../constants/firebase_paths'
import * as postsTools from '../tools/posts_tools'

export const handleNewPost =
    functions 
    .database
    .ref(`${paths.PATH_POSTS}/{postId}`)
    .onCreate(async (change, context) => {

        const post = change.val()
        const postId = context.params.postId
        const groupId = post.id_group
        const recipients = post.recipients
        const timestamp = post.timestamp
        const timestampEv = post.timestampEvent

        const tasks = []

        tasks.push(postsTools.writeContent(postId,groupId,recipients,timestamp,timestampEv))
        // enviar notificaciones
        tasks.push(postsTools.sendNewPostNotification(groupId,recipients))

        try {
            await Promise.all(tasks)
        } catch (error) {
            console.log(error)
        }
    })

export const handleDeletePost =
    functions 
    .database
    .ref(`${paths.PATH_POSTS}/{postId}`)
    .onDelete(async (change, context) => {

        const post = change.val()
        const postId = context.params.postId
        const groupId = post.id_group
        
        try {
            await postsTools.deletePost(groupId,postId)
        } catch (error) {
            console.log(error)
        }
    })