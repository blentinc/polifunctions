import * as functions from 'firebase-functions'
import * as paths from '../constants/firebase_paths'
import * as commentsTools from '../tools/replies_tools'

export const handleNewReply =
functions
.database
.ref(`${paths.PATH_REPLIES}/{postId}/{replyId}`)
.onCreate(async (change, context) => {

    const postId = context.params.postId
    try {
        const reply = change.val()
        const replyUid = reply.id_author
        await commentsTools.sendNewReplyNotification(postId,replyUid)
    } catch (error) {
        console.log(error)
    }
})