import * as admin from 'firebase-admin'
import * as functions from 'firebase-functions'

admin.initializeApp(functions.config().firebase);

export {handleCreatedPhoneInPhoneBook, handleDeletePhoneInPhoneBook} from './functions/phone_book_functions'
export {handleNewPhoneInPhoneActiveUsers} from './functions/phone_users_functions'
export {handleUsernameUpdated} from './functions/people_functions'
export {handleNewMemberInGroup, handleAdminPromotion, handleRemoveMemberFromGroup, handleDeleteGroup} from './functions/group_functions'
export {handleNewPost, handleDeletePost} from './functions/posts_functions'
export {handleNewReply} from './functions/replies_functions'


