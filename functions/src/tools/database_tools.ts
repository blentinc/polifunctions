import * as admin from 'firebase-admin'
import * as tools from './tools'
const db = admin.database()

export async function runMultiPathUpdate(multiPathObj: any) {
    const tag = "run multipath update"
    tools.logFunctionCalled(tag)
    try {
        await db.ref().update(multiPathObj)
    } catch (error) {
        tools.logErrorInFunction(tag,error)
        return error
    }
}

export async function setValueInPath(path:any,value:any) {
    const tag = "setValueInPath"
    tools.logFunctionCalled(tag)

    try {
        await db.ref().child(path).set(value)                      
    } catch (error) {
        tools.logErrorInFunction(tag,error)
        return error
    }   
}

export async function runAddTransactionInPath(path:any) {
    const tag = "runAddTransactionInPath"
    tools.logFunctionCalled(tag)
    try {
        await db.ref(path).transaction(count => { return (count || 0) + 1 })
    } catch (error) {
        tools.logErrorInFunction(tag,error)
        return error
    }
}

export async function runRemoveTransactionInPath(path:any) {
    const tag = "runRemoveTransactionInPath"
    tools.logFunctionCalled(tag)
    try {
        await db.ref(path).transaction(count => { 
            if (count > 0) {
                return count - 1 
            } else { return 0}
        })
    } catch (error) {
        tools.logErrorInFunction(tag,error)
        return error
    }
}

export async function getOnceValueFromPath(path:any) {
    const tag = "getOnceValueFromPath"
    tools.logFunctionCalled(tag)

    try {
        const query = db.ref().child(path)                       
        const result =  await query.once('value')
        return result.val()
       
    } catch (error) {
        tools.logErrorInFunction(tag,error)
        return error
    }   
}

export async function getOnceValueFromPathLimited(path:any,limit:any) {
    const tag = "getOnceValueFromPath"
    tools.logFunctionCalled(tag)

    try {
        const query = db.ref()
                        .child(path)
                        .orderByKey()
                        .limitToLast(limit) 

        const result =  await query.once('value')
        
        return result.val()
       
    } catch (error) {
        tools.logErrorInFunction(tag,error)
        return error
    }   
}

