import * as admin from 'firebase-admin'
import * as paths from '../constants/firebase_paths'
import * as dbTools from './database_tools'
import * as tools from './tools'
import * as poliConst from '../constants/poli_constats'
import * as pushTools from './push_notifications_tools'

export async function getUserTokens(uid:string) {
    const tag = "getUserTokens"
    tools.logFunctionCalled(tag)
    try {
        const path = `${paths.PATH_TOKENS}/${uid}`
        const value = await dbTools.getOnceValueFromPath(path)

        if (value) {
            return value
        } else {
            return null
        }

    } catch (error) {
        tools.logErrorInFunction(tag,error)
        return error
    }
}

export async function subscribeToTopic(topic:string, uid:string) {
    const tag = "subscribeTokenToTopic"
    tools.logFunctionCalled(tag)
    try {
        const tokenValue = await getUserTokens(uid)
        const tokens = Object.keys(tokenValue)
        const result = await admin.messaging().subscribeToTopic(tokens, topic)
        tools.logMsgValue(`Subscribed to topic ${topic}`,result)
    } catch (error) {
        tools.logErrorInFunction(tag,error)
        return error
    }
}

export async function unsubscribeFromTopic(topic:string,uid:string) {

    const tag = "unsubscribeFromTopic"
    tools.logFunctionCalled(tag)
    try {
        const tokenValue = await getUserTokens(uid)
        const tokens = Object.keys(tokenValue)
        await admin.messaging().unsubscribeFromTopic(tokens, topic)
    } catch (error) {
        tools.logErrorInFunction(tag,error)
        return error
    }

}

export async function sendNotification(type:poliConst.notificationType,uid:string, groupId:any, uidReply:string) {

    const tag = "sendNotification"
    tools.logFunctionCalled(tag)

    try {

        const tokenObject = await getUserTokens(uid)

        for (const token in tokenObject) {
            const lang = tokenObject[token]
            const notificationObj = await pushTools.getNotificationTitleAndMsg(lang,type,groupId,uidReply)
            await sendFCMNotification(uid,token,notificationObj[poliConst.KEY_TITLE],notificationObj[poliConst.KEY_MSG])
        }
        
    } catch (error) {
        tools.logErrorInFunction(tag,error)
        return error
    }
}

export async function sendFCMNotification(uid:String,token:any,title:any,msg:any) {

    const tag = "sendFCMNotification"
    tools.logFunctionCalled(tag)

    const payload = {
        notification: {
            title: `${title}`,
            body: `${msg}`,
            sound: 'default',
            badge: "1"
        }
    }

    try {
        const notificationSend = await admin.messaging().sendToDevice(token,payload)
        const results = notificationSend.results
        const multiPathDeleteObj = {} as any

        results.forEach((result, index) => {

            const error = result.error

            if (error) {
                console.error('Failure sending notification to', token, error);
                // Cleanup the tokens who are not registered anymore.
                if (error.code === 'messaging/invalid-registration-token' ||
                    error.code === 'messaging/registration-token-not-registered') {
                    multiPathDeleteObj[`${paths.PATH_TOKENS}/${uid}/${token}`] = null
                }
            } else {
                tools.logMsgValue(`Success sending push notification to ${uid}`,`Msg => ${msg}`)
            }
        })
        
        await dbTools.runMultiPathUpdate(multiPathDeleteObj)

    } catch (error) {
        tools.logErrorInFunction(tag,error)
        return error
    }

    


}