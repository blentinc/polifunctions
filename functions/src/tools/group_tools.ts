import * as paths from '../constants/firebase_paths'
import * as poliConst from '../constants/poli_constats'
import * as tools from './tools'
import * as dbTools from './database_tools'
import * as fcmTools  from './fcm_tools'

export async function getMyGroups(uid: String) {

    const tag = "getPhoneIndexdedUids"
    tools.logFunctionCalled(tag)

    try {
        const path = `${paths.PATH_MY_GROUPS}/${uid}`
        return await dbTools.getOnceValueFromPath(path);
    } catch (error) {
        tools.logErrorInFunction(tag, error)
        return error
    }
}

export async function writeGroupToMyGroups(uid: string, groupId: string, value: string) {

    const tag = "writeGroupToMyGroups"
    tools.logFunctionCalled(tag)

    try {
        const path = `${paths.PATH_MY_GROUPS}/${uid}/${groupId}`
        await dbTools.setValueInPath(path, value);
    } catch (error) {
        tools.logErrorInFunction(tag, error)
        return error
    }

}

export async function removePersonFromGroup(uid: string, groupId: any) {

    const tag = "removeGroupFromMyGroups"
    tools.logFunctionCalled(tag)

    try {
        const path1 = `${paths.PATH_MY_GROUPS}/${uid}/${groupId}`
        const path2 = `${paths.PATH_GROUP_ADMINS}/${groupId}/${uid}`

        const deleteObj = {} as any

        deleteObj[path1] = null
        deleteObj[path2] = null

        await dbTools.runMultiPathUpdate(deleteObj)

    } catch (error) {
        tools.logErrorInFunction(tag, error)
        return error
    }
}

export async function addMemberCount(groupId: any) {
    const tag = "addMemberCount"
    tools.logFunctionCalled(tag)
    try {

        const path = `${paths.PATH_GROUPS}/${groupId}/${paths.KEY_MEMBER_COUNT}`
        await dbTools.runAddTransactionInPath(path)

    } catch (error) {
        tools.logErrorInFunction(tag, error)
        return error
    }
}

export async function addMyGroupsCount(uid: any) {
    const tag = "addMyGroupsCount"
    tools.logFunctionCalled(tag)
    try {

        const path = `${paths.PATH_PEOPLE}/${uid}/${paths.KEY_MY_GROUPS_COUNT}`
        await dbTools.runAddTransactionInPath(path)

    } catch (error) {
        tools.logErrorInFunction(tag, error)
        return error
    }
}

export async function removeMyGroupsCount(uid: any) {
    const tag = "addMyGroupsCount"
    tools.logFunctionCalled(tag)
    try {

        const path = `${paths.PATH_PEOPLE}/${uid}/${paths.KEY_MY_GROUPS_COUNT}`
        await dbTools.runRemoveTransactionInPath(path)

    } catch (error) {
        tools.logErrorInFunction(tag, error)
        return error
    }
}

export async function removeMemberCount(groupId: any) {
    const tag = "removeMemberCount"
    tools.logFunctionCalled(tag)
    try {

        const path = `${paths.PATH_GROUPS}/${groupId}/${paths.KEY_MEMBER_COUNT}`
        await dbTools.runRemoveTransactionInPath(path)

    } catch (error) {
        tools.logErrorInFunction(tag, error)
        return error
    }
}

export async function addAdminToAdminIndexOfGroup(uid: string, groupId: string, value: string) {
    const tag = "removeMemberCount"
    tools.logFunctionCalled(tag)

    try {
        if (value === poliConst.ROL_ADMIN) {
            const path = `${paths.PATH_GROUP_ADMINS}/${groupId}/${uid}`
            await dbTools.setValueInPath(path, value)
        }
    } catch (error) {
        tools.logErrorInFunction(tag, error)
        return error
    }
}

export async function getGroupMembers(groupId: any) {
    const tag = "getGroupMembers"
    tools.logFunctionCalled(tag)

    try {
        const members = await dbTools.getOnceValueFromPath(`${paths.PATH_GROUP_MEMBERS}/${groupId}`)
        if (members) {
            return Object.keys(members)
        } else {
            return null
        }

    } catch (error) {
        tools.logErrorInFunction(tag, error)
        return error
    }
}

export async function getGroupInfo(groupId: any) {
    const tag = "getGroupInfo"
    tools.logFunctionCalled(tag)
    try {
        const group = await dbTools.getOnceValueFromPath(`${paths.PATH_GROUPS}/${groupId}`)

        if (group) {
            return group
        } else {
            return null
        }

    } catch (error) {
        tools.logErrorInFunction(tag, error)
        return error
    }
}

export async function writeGroupPostsToNewMember(uid: any, groupId: any) {
    const tag = "getGroupInfo"
    tools.logFunctionCalled(tag)

    try {

        const posts = await getGroupPosts(groupId, poliConst.postType.public, 50)
        const events = await getGroupEvents(groupId, poliConst.postType.public, 50)
        const tasks = [] as any

        if (posts) {
            setPosts(posts, tasks, uid)
        }

        if (events) {
            setEvents(events, tasks, uid)
        }

        await Promise.all(tasks)

    } catch (error) {
        tools.logErrorInFunction(tag, error)
        return error
    }
}

function setPosts(posts: any, tasks: any[], uid: any) {
    for (const post in posts) {
        const timestamp = posts[post]
        tasks.push(dbTools.setValueInPath(`${paths.PATH_FEED}/${uid}/${post}`, timestamp))
    }
}

function setEvents(events: any, tasks: any[], uid: any) {
    for (const event in events) {
        const timestamp = events[event]
        tasks.push(dbTools.setValueInPath(`${paths.PATH_FEED_EVENTS}/${uid}/${event}`, timestamp))
    }
}

export async function getGroupPosts(groupId: any, type: poliConst.postType, limit: any) {
    const tag = "getGroupPosts"
    tools.logFunctionCalled(tag)

    let path = ``

    switch (+type) {
        case poliConst.postType.public:
            path = `${paths.PATH_POSTS_GROUP_PUB}/${groupId}`
            break
        case poliConst.postType.private:
            path = `${paths.PATH_POSTS_GROUP_PRIV}/${groupId}`
            break
        case poliConst.postType.general:
            path = `${paths.PATH_POSTS_GROUP}/${groupId}`
            break
    }

    try {
        const data = await dbTools.getOnceValueFromPathLimited(path, limit)

        if (data) {
            return data
        } else {
            return null
        }

    } catch (error) {
        tools.logErrorInFunction(tag, error)
        return error
    }

}

export async function getGroupEvents(groupId: any, type: poliConst.postType, limit: any) {
    const tag = "getGroupEvents"
    tools.logFunctionCalled(tag)

    let path = ``

    switch (+type) {
        case poliConst.postType.public:
            path = `${paths.PATH_POSTS_GROUP_EV_PUB}/${groupId}`
            break
        case poliConst.postType.private:
            path = `${paths.PATH_POSTS_GROUP_EV_PRIV}/${groupId}`
            break
        case poliConst.postType.general:
            path = `${paths.PATH_POSTS_GROUP_EV}/${groupId}`
            break
    }


    try {
        const data = await dbTools.getOnceValueFromPathLimited(path, limit)

        if (data) {
            return data
        } else {
            return null
        }

    } catch (error) {
        tools.logErrorInFunction(tag, error)
        return error
    }

}

export async function deleteGroup(groupId: any, group: any) {
    
    const tag = "deleteGroup"
    tools.logFunctionCalled(tag)
    
    const tasks = []
    const multiPathObj = {} as any

    try {

        const members = await getGroupMembers(groupId)

        for (const uid of members) {
            tasks.push(removeMyGroupsCount(uid))
            multiPathObj[`${paths.PATH_MY_GROUPS}/${uid}/${groupId}`] = null
        }
        
        multiPathObj[`${paths.PATH_GROUPS}/${groupId}`] = group
        tasks.push(dbTools.runMultiPathUpdate(multiPathObj))

        await Promise.all(tasks)

    } catch (error) {
        tools.logErrorInFunction(tag, error)
        return error
    }

}

export async function sendWelcomeNotification(groupId:any, uid:any) {

    const tag = "sendWelcomeNotification"
    tools.logFunctionCalled(tag)

    try {
        await fcmTools.sendNotification(poliConst.notificationType.added,uid,groupId,"")
    } catch (error) {
        tools.logErrorInFunction(tag, error)
        return error
    }
}

export async function sendAdminNotification(value:any, groupId:any, uid:any) {
    const tag = "sendAdminNotification"
    tools.logFunctionCalled(tag)

    try {
        if (value === poliConst.ROL_ADMIN) {
            await fcmTools.sendNotification(poliConst.notificationType.admin,uid,groupId,"")
        }
    } catch (error) {
        tools.logErrorInFunction(tag, error)
        return error
    }
}

