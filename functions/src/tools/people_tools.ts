import * as admin from 'firebase-admin'
import * as paths from '../constants/firebase_paths'
import * as tools from './tools'
import * as poliConst from '../constants/poli_constats'
import * as dbTools from './database_tools'
const db = admin.database();

export async function writePersonToMyContacts(uid: string, uidContact:string) {
    const tag = "write person to my contacts"
    try {
        
        tools.logFunctionCalled(tag)

        const user = await getPersonInfo(uidContact)
        
        if (!user) {
            tools.logMsgValue("Error person does not exists in poli",user)
            return
        }

        let userLowName = user.name_lowercased
        
        if (!userLowName) {
            userLowName = "no name"
        }

        const multiPathObj = {} as any

        multiPathObj[`${paths.PATH_MY_CONTACTS}/${uid}/${uidContact}`] = tools.removeSpacesFromWord(userLowName)
        multiPathObj[`${paths.PATH_MY_CONTACTS_INDEX}/${uidContact}/${uid}`] = true
        
        await dbTools.runMultiPathUpdate(multiPathObj)

    } catch (error) {
        tools.logErrorInFunction(tag,error)
        return error
    }
}

export async function deletePersonOfMyContacts(uid: string, uidContact:string) {
    const tag = "delete person of my contacts"
    try {
        
        tools.logFunctionCalled(tag)

        await db.ref().child(`${paths.PATH_MY_CONTACTS}/${uid}/${uidContact}`).set(null)
        await db.ref().child(`${paths.PATH_MY_CONTACTS_INDEX}/${uidContact}/${uid}`).set(null)

    } catch (error) {
        tools.logErrorInFunction(tag,error)
        return error
    }
}

export async function getMyContactsIndex(uid:String) {
    const tag = "Get my contacts index"
    try {
        tools.logFunctionCalled(tag)

        const query = db.ref().child(`${paths.PATH_MY_CONTACTS_INDEX}/${uid}`)
        const response =  (await query.once('value')).val()
        if (response) {

            return Object.keys(response)

        } else {return null}
        
    } catch (error) {
        tools.logErrorInFunction(tag,error)
        return error
    }
}

export async function getPersonInfo(uid: string) {
    const tag = "Get person info"
    try {
        tools.logFunctionCalled(tag)
        const query = db.ref().child(`${paths.PATH_PEOPLE}/${uid}`)
        const user =  (await query.once('value')).val()

        if (user) {
            tools.logMsgValue('User retreived',user)
            return user
        } else {
            return null
        }
        
    } catch (error) {
        tools.logErrorInFunction(tag,error)
        return error
    }
}

export function updateNameInMyContacts(uidContact:any,uids:any,nameJoined:any,multiPathObj:any) {
    const tag = "updateNameInMyContacts"
    for(const uid of uids) {
        multiPathObj[`${paths.PATH_MY_CONTACTS}/${uid}/${uidContact}`] = nameJoined
    }
    tools.logFunctionCalled(tag)
}

export function updateNameInMyGroups(uid:any,groupIds:any,nameJoined:any,multiPathObj:any) {
    const tag = "updateNameInMyGroups"
    for (const groupId in groupIds) {
        const rol = groupIds[groupId]
        if (rol !== poliConst.ROL_ADMIN && rol !== poliConst.ROL_FOUNDER) {
            multiPathObj[`${paths.PATH_GROUP_MEMBERS}/${groupId}/${uid}`] = nameJoined
        }
    }
    tools.logFunctionCalled(tag)
}


