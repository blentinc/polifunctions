import * as admin from 'firebase-admin'
import * as tools from './tools'
const db = admin.database();
import * as paths from '../constants/firebase_paths'

export async function checkPhoneStatus(phone: string) {

    const tag = "check phone status"
    tools.logFunctionCalled(tag)
    try {
        
        const query = db.ref().child(`${paths.PATH_PHONES}`)
                              .orderByValue()
                              .equalTo(phone)
        
        const result =  (await query.once('value')).val()

        if (result) {
            const uid = Object.keys(result)
            console.log('This phone is being used by => uid ',uid)
            return uid
        } else {
            return null
        }
        
    } catch (error) {
        tools.logErrorInFunction(tag,error)
        return error
    }
}


export async function writePhoneToPhoneBookIndex(phone: string, uid: string) {

    const tag = "writePhoneToPhoneBookIndex"
    tools.logFunctionCalled(tag)

    try {
        await db.ref().child(`${paths.PATH_PHONE_BOOK_INDEX}/${phone}/${uid}`).set(true)        
    } catch (error) {
        tools.logErrorInFunction(tag,error)
        return error
    }
}

export async function deletePhoneOfPhoneBookIndex(phone: string, uid: string) {

    const tag = "deletePhoneOfPhoneBookIndex"
    tools.logFunctionCalled(tag)

    try {
        await db.ref().child(`${paths.PATH_PHONE_BOOK_INDEX}/${phone}/${uid}`).set(null)        
    } catch (error) {
        tools.logErrorInFunction(tag,error)
        return error
    }
}

export async function getPhoneIndexdedUids(phone:String) {

    const tag = "getPhoneIndexdedUids"
    tools.logFunctionCalled(tag)

    try {

        const path = `${paths.PATH_PHONE_BOOK_INDEX}/${phone}`

        const query = db.ref().child(path)                       
        const result =  await query.once('value')
        const values = result.val()
  
        if (values) {
            const uids = Object.keys(values)
            console.log(`there are ${uids.length} with this phone as contact`)
            return uids
        } else {
            return null
        }
        
    } catch (error) {
        tools.logErrorInFunction(tag,error)
        return error
    }

}

