import * as paths from '../constants/firebase_paths'
import * as poliConst from '../constants/poli_constats'
import * as tools from './tools'
import * as dbTools from './database_tools'
import * as grouptools from './group_tools'
import * as fcmTools  from './fcm_tools'


export async function writeContent(postId:any,groupId:any,recipients:any,timestampPost:any,timestampEvent:any) {
    const tag = "writeContent"
    tools.logFunctionCalled(tag)


    const timestamp = -1 * timestampPost
    const multiPathObject = {} as any

    try {

        const uids = await defineRecipients(recipients,groupId)

        for (const uid of uids) {
           
            if (timestampEvent) {
                multiPathObject[`${paths.PATH_FEED_EVENTS}/${uid}/${postId}`] = timestampEvent
            }
    
            multiPathObject[`${paths.PATH_FEED}/${uid}/${postId}`] = timestamp
            multiPathObject[`${paths.PATH_FEED_BY_GROUP}/${uid}/${groupId}/${postId}`] = timestamp
        }

        await dbTools.runMultiPathUpdate(multiPathObject)

    } catch (error) {
        tools.logErrorInFunction(tag,error)
        return error
    }
}

export async function sendNewPostNotification(groupId:any,recipients:any) {

    const tag = "sendNewPostPushNotification"
    tools.logFunctionCalled(tag)

    try {
        const uids = await defineRecipients(recipients,groupId)
        // for in loops in the properties, for of loops the values
        for(const uid of uids) {
            await fcmTools.sendNotification(poliConst.notificationType.post,uid,groupId,"")
        }
        
    } catch (error) {
        tools.logErrorInFunction(tag,error)
        return error
    }
}

async function defineRecipients(recipients:any,groupId:any) {

    const tag = "setRecipientsOfPost"
    tools.logFunctionCalled(tag)

    const uids = Object.keys(recipients)
    const firstKey = uids[0]
    try {
        if (firstKey === poliConst.KEY_ALL && uids.length === 1) {
            return await grouptools.getGroupMembers(groupId)
        } else {
            return uids
        }
    } catch (error) {
        tools.logErrorInFunction(tag,error)
        return error
    }
}

export async function getPostInfo(postId:any) {

    const tag = "getPostInfo"
    tools.logFunctionCalled(tag)

    try {
        return await dbTools.getOnceValueFromPath(`${paths.PATH_POSTS}/${postId}`)
    } catch (error) {
        tools.logErrorInFunction(tag,error)
        return error
    }

}

export async function deletePost(groupId:any,postId:any) {

    const tag = "deletePost"
    tools.logFunctionCalled(tag)

    try {
        const members = await grouptools.getGroupMembers(groupId)
        const multiPathObject = {} as any
        
        for (const uid of members) {
            multiPathObject[`${paths.PATH_FEED}/${uid}/${postId}`] = null
            multiPathObject[`${paths.PATH_FEED_EVENTS}/${uid}/${postId}`] = null
        }

        multiPathObject[`${paths.PATH_POSTS_GROUP}/${groupId}/${postId}`] = null
        multiPathObject[`${paths.PATH_POSTS_GROUP_PRIV}/${groupId}/${postId}`] = null
        multiPathObject[`${paths.PATH_POSTS_GROUP_PUB}/${groupId}/${postId}`] = null
        multiPathObject[`${paths.PATH_POSTS_GROUP_EV}/${groupId}/${postId}`] = null
        multiPathObject[`${paths.PATH_POSTS_GROUP_EV_PRIV}/${groupId}/${postId}`] = null
        multiPathObject[`${paths.PATH_POSTS_GROUP_EV_PUB}/${groupId}/${postId}`] = null

        await dbTools.runMultiPathUpdate(multiPathObject)

    } catch (error) {
        tools.logErrorInFunction(tag,error)
        return error
    }
}
