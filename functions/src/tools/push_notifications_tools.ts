import * as poliConst from '../constants/poli_constats'
import * as groupTools from '../tools/group_tools'
import * as peopleTools  from '../tools/people_tools'
import * as tools from './tools'


export async function getNotificationTitleAndMsg(lang:any,type:poliConst.notificationType,groupId:any,uid:string) {
    const tag = "getNotificationTitleAndMsg"
    tools.logFunctionCalled(tag)
    try {

        switch (+type) {
            case poliConst.notificationType.post:
                return await getNewPostPNM(lang,groupId)
            case poliConst.notificationType.reply:
                return await getNewReplyPNM(lang,uid)
            case poliConst.notificationType.added:
                return await getNewMemberInGroupPNM(lang,groupId)
            case poliConst.notificationType.admin:
                return await getAdminPromotionPNM(lang,groupId)
        }
        
    } catch (error) {
        tools.logErrorInFunction(tag,error)
    }
    
}

export async function getNewPostPNM(lang:any, groupId:any) {
    const tag = "getNewPostNotificationPush"
    tools.logFunctionCalled(tag)

    const notificationInfo = {} as any
    let groupName = ""

    try {
        const group = await groupTools.getGroupInfo(groupId)
        groupName = group.name

        switch(lang) {
            case 'es':
                notificationInfo[poliConst.KEY_MSG] = '📩 Has recibido una nueva publicación de '+groupName 
                notificationInfo[poliConst.KEY_TITLE] = 'Nueva publicación 📣'
                break
            default:
                notificationInfo[poliConst.KEY_MSG] = '📩 You have a new post from '+groupName 
                notificationInfo[poliConst.KEY_TITLE] = 'New post 📣'
                break
        }
    
        return notificationInfo

    } catch (error) {
        tools.logErrorInFunction(tag,error)
        return error
    }
}

export async function getNewReplyPNM(lang:any, uid:string) {

    const tag = "getNewCommentNotificationPush"
    tools.logFunctionCalled(tag)

    const notificationInfo = {} as any
    let name = ""
    
    try {
        let person = await peopleTools.getPersonInfo(uid)
        name = person.name
    } catch (error) {
        tools.logErrorInFunction(tag,error)
    }

    switch(lang) {
        case 'es':
            notificationInfo[poliConst.KEY_MSG] = `📝 ${name} te ha escrito una respuesta`
            notificationInfo[poliConst.KEY_TITLE] = 'Nueva respuesta 💬'
            break
        default:
            notificationInfo[poliConst.KEY_MSG] = `📝 ${name} sent you a new reply`
            notificationInfo[poliConst.KEY_TITLE] = 'New reply 💬'
            break
    }

    return notificationInfo
}

export async function getAdminPromotionPNM(lang:any, groupId:any) {
    const tag = "getAdminPromotionPNM"
    tools.logFunctionCalled(tag)

    const notificationInfo = {} as any
    let groupName = ""

    try {
        const group = await groupTools.getGroupInfo(groupId)
        groupName = group.name

        switch(lang) {
            case 'es':
                notificationInfo[poliConst.KEY_MSG] = '🔝 Ahora eres administrador en '+groupName
                notificationInfo[poliConst.KEY_TITLE] = 'Felicitaciones 🎉!'
                break
            default:
                notificationInfo[poliConst.KEY_MSG] = '🔝 You are now an admin in '+groupName
                notificationInfo[poliConst.KEY_TITLE] = 'Congrats 🎉!'
                break
        }
    
        return notificationInfo

    } catch (error) {
        tools.logErrorInFunction(tag,error)
        return error
    }
}

export async function getNewMemberInGroupPNM(lang:any, groupId:any) {
    const tag = "getNewPostNotificationPush"
    tools.logFunctionCalled(tag)

    const notificationInfo = {} as any
    let groupName = ""

    try {
        const group = await groupTools.getGroupInfo(groupId)
        groupName = group.name

        switch(lang) {
            case 'es':
                notificationInfo[poliConst.KEY_MSG] = '👏 Ahora eres parte de '+groupName
                notificationInfo[poliConst.KEY_TITLE] = 'Bienvenido 🥳!'
                break
            default:
                notificationInfo[poliConst.KEY_MSG] = '👏 Now you belong to '+groupName
                notificationInfo[poliConst.KEY_TITLE] = 'Welcome 🥳!'
                break
        }
    
        return notificationInfo

    } catch (error) {
        tools.logErrorInFunction(tag,error)
        return error
    }
}

