
import * as poliConst from '../constants/poli_constats'
import * as tools from './tools'
import * as fcmTools  from './fcm_tools'
import * as postTools from './posts_tools'

export async function sendNewReplyNotification(postId:any, uidReply:string) {

    const tag = "sendNewCommentNotification"
    tools.logFunctionCalled(tag)

    try {

        const post = await postTools.getPostInfo(postId)
        const uid = post.id_author
        const type = poliConst.notificationType.reply

        await fcmTools.sendNotification(type,uid,null,uidReply)
    
    } catch (error) {
        tools.logErrorInFunction(tag,error)
    }
}