export function logMsgValue(message:string,value:any ) {
    console.log(`\n${message} => `,value)
}

export function logFunctionCalled(tag:string) {
    console.log(`\nFunction tool called=> ${tag}`)
}

export function logErrorInFunction(tag:string, error:any) {
  console.error(`\nError in function tool called => ${tag}\nError => `,error)
}

export function removeSpecialCharsFromWord(word: string) {
  
    return word.normalize('NFD')
    .replace(/[\u0300-\u036f]/g, "")
   
}

export function removeSpacesFromWord(word: string) {
    
    return word.trim().split(/\s+/).join("")
     
}

export function isObjectEmpty(obj:any) {
  for (const key in obj) {
      if (obj.hasOwnProperty(key))
          return false;
  }
  return true;
}

/**
 * 
 * 
exports.normalizeWord = function (word) {
 
}

exports.removeSpacesFromMail = function (word) {
  var str = String(word);
  // Normalize the string and lowercased
  var myNameNormalized = str.trim().toLowerCase();
  var mail = myNameNormalized.split(/\s+/).join("")


  return mail;
}

exports.removeNonAlphanumericFromWord = function (word) {
  var str = String(word);

  var wordWithoutNonAlphaNum = word.replace(/\W/g, '')

  return wordWithoutNonAlphaNum;
};

exports.formatedNameFirstCharUpper = function (name) {

  const partsOfName = name.split(" ");
  const arrayNewName = []

  partsOfName.forEach(part => {
    arrayNewName.push(jsUcfirst(part))
  })

  const newName = arrayNewName.join(" ");
  return newName;

}

function jsUcfirst(string) {

  return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();

}

exports.combineComplexObjects = function (obj, container) {
  for (let key of Object.keys(obj)) {
    if (!container[key]) container[key] = {};

    for (let innerKey of Object.keys(obj[key]))
      container[key][innerKey] = obj[key][innerKey];
  }
}

exports.combineIndexedObjects = function (obj, container) {
  for (let key of Object.keys(obj)) {
    if (!container[key]) container[key] = {};

    container[key] = obj[key]
  }
}

exports.indexedObjectNestedTrueValue = function (obj, container, property) {
  for (let key of Object.keys(obj)) {
    if (!container[key]) container[key] = {};

    const value = {}

    value[obj[key][property]] = true

    container[key] = value
  }
}


exports.indexedObjectByProperty = function (obj, container, property) {
  for (let key of Object.keys(obj)) {
    if (!container[key]) container[key] = {};

    container[key] = obj[key][property]
  }
}

exports.indexedObjectByKeyAsValue = function (obj, container) {
  for (let key of Object.keys(obj)) {
    if (!container[key]) container[key] = {};

    container[key] = key
  }
}

exports.objectEmpty = function (obj) {
  for(var key in obj) {
      if(obj.hasOwnProperty(key))
          return false;
  }
  return true;
}




 */